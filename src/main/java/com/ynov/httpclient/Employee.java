package com.ynov.httpclient;

import java.util.Objects;

public class Employee {

    private Integer id;

    private String nom;

    private String prenom;

    private Boolean isManager;

    private Long managerId = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Boolean getIsManager() {
        return isManager;
    }

    public void setIsManager(Boolean manager) {
        isManager = manager;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee employee)) return false;
        return Objects.equals(getId(), employee.getId()) && Objects.equals(getNom(), employee.getNom()) && Objects.equals(getPrenom(), employee.getPrenom()) && Objects.equals(getIsManager(), employee.getIsManager()) && Objects.equals(getManagerId(), employee.getManagerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNom(), getPrenom(), getIsManager(), getManagerId());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", isManager=" + isManager +
                ", managerId=" + managerId +
                '}';
    }
}
