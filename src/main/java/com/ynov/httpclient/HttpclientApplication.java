package com.ynov.httpclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class HttpclientApplication {

    public static void main(String[] args) {
        WebClient client = WebClient.create("http://localhost:8080");

        String response = client.get()
                .uri("/employees")
                .retrieve()
                .bodyToMono(String.class)
                .block();

        System.out.println(response);

        Employee empl = new Employee();
        empl.setNom("Voila");
        empl.setPrenom("JeSuis");
        empl.setIsManager(false);
        empl.setManagerId(1L);

        Employee createdEmployee = client.post()
                .uri("/employees")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(empl), Employee.class)
                .retrieve()
                .bodyToMono(Employee.class)
                .block();

        System.out.println(createdEmployee);

        String response2 = client.get()
                .uri("/employees")
                .retrieve()
                .bodyToMono(String.class)
                .block();

        System.out.println(response2);
    }

}
